//
//  AppDelegate.swift
//  MyFirstRealm
//
//  Created by Masatoshi Nishikata on 2/03/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch.
			return true
	}



}



