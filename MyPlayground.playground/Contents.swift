//: Playground - noun: a place where people can play

import UIKit

import Foundation

// Compressing and decompressing a UUID to 22 characters via base64.
// Works great as a Swift playground. These articles were helpful:
// http://blog.codinghorror.com/equipping-our-ascii-armor/
// http://69.195.124.60/~jasondoh/2013/08/14/creating-a-short-guid-in-objective-c/

let identifier = NSUUID().UUIDString
let base64TailBuffer = "="

func compressIdentifier(id: String?) -> String? {
	let base64TailBuffer = "="
	
	guard let identifier = id else {
		return nil
	}
	
	guard let tempUuid = NSUUID(UUIDString: identifier) else {
		return nil
	}
	
	var tempUuidBytes = [UInt8](count: 16, repeatedValue: 0)
	tempUuid.getUUIDBytes(&tempUuidBytes)
	let data = NSData(bytes: &tempUuidBytes, length: 16)
	let base64 = data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
	return base64.stringByReplacingOccurrencesOfString(base64TailBuffer, withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
}

func rehydrate(shortenedIdentifier: String?) -> String? {
	// Expand an identifier out of a CBAdvertisementDataLocalNameKey or service characteristic.
	if shortenedIdentifier == nil {
		return nil
	}
	else {
		// Rehydrate the shortenedIdentifier
		let shortenedIdentifierWithDoubleEquals = shortenedIdentifier! + base64TailBuffer + base64TailBuffer
		let data = NSData(base64EncodedString: shortenedIdentifierWithDoubleEquals, options: NSDataBase64DecodingOptions())
		let tempUuid = NSUUID(UUIDBytes: UnsafePointer<UInt8>(data!.bytes))
		return tempUuid.UUIDString
	}
}

let testCompress = compressIdentifier(identifier)
let testRehydrate = rehydrate(testCompress)