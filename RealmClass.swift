//
//  RealmClass.swift
//  MyFirstRealm
//
//  Created by Masatoshi Nishikata on 2/03/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import RealmSwift
import CoreData

class Dog:ManagedObject {
	
	dynamic var name:String = ""

	// Strange deifinition that comes in shape of Swift grammer
	override static func indexedProperties() -> [String] {
		return ["name"]
	}

	// Migrating from CoreData
	
	func migrateFromCoreDataManagedObject(object:NSManagedObject) {
		self.name = object.valueForKey("name") as! String
		self.uuid = object.valueForKey("uuid") as! String

	}
}

func shortUUIDString() -> String {
	
	let base64TailBuffer = "="
	let tempUuid = NSUUID()
	var tempUuidBytes = [UInt8](count: 16, repeatedValue: 0)
	tempUuid.getUUIDBytes(&tempUuidBytes)
	let data = NSData(bytes: &tempUuidBytes, length: 16)
	let base64 = data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
	
	let string = base64.stringByReplacingOccurrencesOfString("/", withString: "_", options: [], range: nil)
	
	return string.stringByReplacingOccurrencesOfString(base64TailBuffer, withString: "", options: [], range: nil)
}


// MARK:- Realm Extensions ... Using like CoreData ManagedObjectID to communicate between threads

// Why do they use the name 'Object'??

class ManagedObject:Object {
	dynamic var uuid:String!
	override static func primaryKey() -> String {
		return "uuid"
	}
}

typealias ManagedObjectID = String

extension Results {
	
	var objectIDs:[ManagedObjectID] {
		
		get {
			guard let primaryKey = T.primaryKey() else {
				print("*** You fetched wrong type ***")
				return []
			}
			var keys:[ManagedObjectID] = []
			for result:T in self {
				guard result is ManagedObject else {
					print("*** You fetched wrong type ***")
					return []
				}
				if let value = result.valueForKey(primaryKey) {
					keys.append(value as! ManagedObjectID)
				}
			}
			return keys
		}
	}
}

enum ManagedObjectError: ErrorType {
	case AlreadyInserted
}

extension Realm {
	
	static var fetchQueue:dispatch_queue_t = {
		let fetchQueue_ = dispatch_queue_create("Fetch Queue", DISPATCH_QUEUE_SERIAL);
		dispatch_set_target_queue(fetchQueue_, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
		return fetchQueue_
	}()
	
	static var defaultRealm:Realm = {
		assert( NSThread.isMainThread() == true, "*** Call from main thread ***")
		
		let realm:Realm!
		do {
			realm = try Realm()
		}catch let error as NSError {
			realm = nil
			print("*** Fatal error \(error)")
		}
		return realm
	}()
	
	func insertObject(object:ManagedObject) throws {
		
		if object.uuid != nil {
			throw ManagedObjectError.AlreadyInserted
		}
		
		object.uuid = shortUUIDString()
		add(object)
	}
	
	func objectsWithObjectIDs<T:ManagedObject>(type: T.Type, keys:[AnyObject]) -> [T] {
		var objs:[T] = []
		for key in keys {
			if let obj = objectForPrimaryKey(T.self, key: key) {
				objs.append(obj)
			}
		}
		return objs
	}
}